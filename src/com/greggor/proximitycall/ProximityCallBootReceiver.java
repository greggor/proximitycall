/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall;

import com.greggor.proximitycall.listeners.ProximitySensorListener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Allows application to be loaded after system boot is completed.
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class ProximityCallBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		//on start, update proximity sensor listener status basing on preferences settings
		ProximitySensorListener.setPersistent(context, prefs.getBoolean("persistent_sensor", false));
	}

}
