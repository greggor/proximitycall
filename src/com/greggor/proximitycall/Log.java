/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall;

import java.util.Date;

/**
 * Logging class with more detailed timestamps and tagging with class name
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public final class Log {
	/**
	 * Application log tag
	 */
	public static final String APP_TAG="ProximityCall";
  
	/**
	 * <i>Log.l</i> wrapper
	 * @param tag method/class tag
	 * @param msg message string (may contain formatting)
	 * @param params parameters passed to message string
	 */
	public static void l(String tag, String msg, Object... params)
    {
		String ts = String.format("[%1$tH:%1$tM:%1$tS,%1$tL]", (new Date()).getTime());
		android.util.Log.i(APP_TAG, String.format("%s %s: %s", ts, tag, String.format(msg,params)));
    }

	/**
	 * <i>Log.d</i> wrapper
	 * @param tag method/class tag
	 * @param msg message string (may contain formatting)
	 * @param params parameters passed to message string
	 */
	public static void d(String tag, String msg, Object... params)
    {
		String ts = String.format("[%1$tH:%1$tM:%1$tS,%1$tL]", (new Date()).getTime());
		android.util.Log.d(APP_TAG, String.format("%s %s: %s", ts, tag, String.format(msg,params)));
    }
}
