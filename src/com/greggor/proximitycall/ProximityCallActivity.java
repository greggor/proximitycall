/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall;

import com.greggor.proximitycall.listeners.ProximitySensorListener;
import com.greggor.proximitycall.preference.SettingsFragment;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;

/**
 * Displays preferences screen
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class ProximityCallActivity extends Activity implements OnSharedPreferenceChangeListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
		//on start, update proximity sensor listener status basing on preferences settings
		ProximitySensorListener.setPersistent(this, sharedPreferences.getBoolean("persistent_sensor", false));
		sharedPreferences.registerOnSharedPreferenceChangeListener(this);
		// load preference using Fragments manner
		getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
	}

	@Override
	protected void onDestroy() {
		PreferenceManager.getDefaultSharedPreferences(this).unregisterOnSharedPreferenceChangeListener(this);
		// destroy proximity sensor listener
		ProximitySensorListener.setPersistent(this, false);
		super.onDestroy();
	}
	
	/*
	 * Handle preferences changes
	 * @see android.content.SharedPreferences.OnSharedPreferenceChangeListener#onSharedPreferenceChanged(android.content.SharedPreferences, java.lang.String)
	 */
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		if (key.equals("persistent_sensor"))
			//update proximity sensor listener status
			ProximitySensorListener.setPersistent(this, prefs.getBoolean(key, false));
		if ((key.equals("sensor_delay") || key.equals("delay")) && prefs.getBoolean("persistent_sensor",false)) {
				//persistent sensor instance needs to be re-created with new sensor delay value
				ProximitySensorListener.setPersistent(this, true, true);
		}
	}
}