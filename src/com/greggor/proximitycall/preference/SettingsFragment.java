/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall.preference;

import com.greggor.proximitycall.R;
import android.os.Bundle;
import android.preference.PreferenceFragment;

/**
 * Preference management using Fragments
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
    }

}