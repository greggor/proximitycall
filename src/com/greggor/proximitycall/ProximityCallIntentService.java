/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall;

import com.greggor.proximitycall.listeners.BluetoothListener;
import com.greggor.proximitycall.listeners.ProximitySensorListener;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

/**
 * Since answering the call may take a while, when an incoming call occurs an intent service is created 
 * to perform actual call pickup 
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class ProximityCallIntentService extends IntentService {
	private static final String TAG = "IntentService";
	/**
	 * Answer call action flag
	 */
	private boolean mAnswerCall = false;
    
	/**
	 * Public constructor
	 */
    public ProximityCallIntentService() {
		super("ProximityCallIntentService");
	}
    
    /**
     * Answer incoming call by simulating virtual key press - it's the only portable way
     * 
     * @param context application context
     */
	private void answerPhoneHeadsethook(Context context) {
		Log.l(TAG, "Using \"headset hook\" to answer the call.");
		// Simulate a press of the headset button to pick up the call
		Intent buttonDown = new Intent(Intent.ACTION_MEDIA_BUTTON);		
		buttonDown.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
		context.sendOrderedBroadcast(buttonDown, "android.permission.CALL_PRIVILEGED");

		// froyo and beyond trigger on buttonUp instead of buttonDown
		Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);		
		buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
		context.sendOrderedBroadcast(buttonUp, "android.permission.CALL_PRIVILEGED");
	}

	/**
	 * Free resources
	 * 
	 * @param proxyListener proximity sensor listener instance
	 * @param btListener Bluetooth headset profile listener instance
	 */
	private void closeListeners(ProximitySensorListener proxyListener, BluetoothListener btListener)
    {
    	if (proxyListener != null)
    		proxyListener.close();
    	if (btListener != null) {
    		btListener.close();
    	}
    }
	
	/**
	 * Do not answer the call with proximity sensor when Bluetooth headset is connected, unless told to do so
	 * @param btListener Bluetooth listener object
	 * @return <b>true</b> if call should be answered, <b>false</b> otherwise
	 */
	private boolean bluetoothHeadsetPreventsAnswer(BluetoothListener btListener) {
		return btListener.isHeadsetConnected() && !PreferenceManager.getDefaultSharedPreferences(this).getBoolean("bt_enabled", false);
		
	}
    
	@SuppressWarnings("deprecation")
	@Override
	/**
	 * Do actual call pickup
	 */
	protected void onHandleIntent(Intent intent) {
		Log.l(TAG, "Starting incoming call handler");
		Context context = getBaseContext();
		// wired headset presence checking
		AudioManager am = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		// BT headset presence checking
		BluetoothListener btListener = new BluetoothListener(context);
		// Proximity sensor adapter
		ProximitySensorListener proxyListener = null;
		TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		try {
			// while phone rings
			while (tm.getCallState() == TelephonyManager.CALL_STATE_RINGING && !mAnswerCall) {
				if (bluetoothHeadsetPreventsAnswer(btListener) || am.isWiredHeadsetOn()) {
					// do not answer the call while any headset connected
					closeListeners(proxyListener, null);
					proxyListener = null;
				} else { 
					if (proxyListener == null)
						proxyListener = ProximitySensorListener.getSensorProxy(context, true);
					if (proxyListener.proximityDetected())
						// sensor covered - assume that phone is put at ear raise the flag to answer the call
						mAnswerCall = true;
				}
				Thread.sleep(100);
			}
		} catch (InterruptedException e) {
			// don't care
		} finally {
			// free resources
			closeListeners(proxyListener, btListener);
		}
		
		// if we got here and the flag is not set, it means that call has been answered/rejected
		// otherwise (virtual button, headset button etc.)
		if (!mAnswerCall) {
			Log.l(TAG, "Phone not ringing, aborting");

			return;
		}

		Log.l(TAG, "Answering the call");

		// make sure the phone is still ringing - TODO unnecessary second check here?
		if (tm.getCallState() != TelephonyManager.CALL_STATE_RINGING) {
			return;
		}

		// answer the call
		answerPhoneHeadsethook(context);
		
		Log.l(TAG, "Incoming call handler completed");
		return;
	}
}
