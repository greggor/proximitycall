/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.preference.PreferenceManager;
import android.telephony.TelephonyManager;

/**
 *  Incoming call handler class
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class ProximityCallReceiver extends BroadcastReceiver {
	private static final String TAG = "IntentService";

	@Override
	public void onReceive(Context context, Intent intent) {

		// Load preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);

		// Check phone state
		String phone_state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);

		if (phone_state.equals(TelephonyManager.EXTRA_STATE_RINGING) && prefs.getBoolean("enabled", false)) {
			// We're not answering the incoming call while currently in the other call. Sorry.
			AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			if (am.getMode() == AudioManager.MODE_IN_CALL) {
				Log.l(TAG, "Already in call - not serving the incoming one");
				return;
			}
			// Call a service, since this could take a few seconds
			context.startService(new Intent(context, ProximityCallIntentService.class));
		}		
	}
}
