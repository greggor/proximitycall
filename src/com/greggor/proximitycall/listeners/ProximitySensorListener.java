/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall.listeners;

import java.util.Date;

import com.greggor.proximitycall.Log;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.preference.PreferenceManager;

/**
 * Analyzes proximity sensor events. Can be polled to check if user has put the phone next to ear
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class ProximitySensorListener implements SensorEventListener {
	private static final String TAG = "ProximitySensorListener";
	/**
	 * When recent sensor event was registered
	 */
    private long mTimestamp = 0;
    /**
     * How long sensor must be uncovered before covering it is treated as putting it next to ear 
     */
    private int mDelay;
    /**
     * Number of NEAR events registered (see {@link #mGalaxyS3Workaround})
     */
    private int mNearCounter = 0;
    /**
     * Sensor events are processed / ignored
     */
    private boolean mAttached = false;
    /**
     * Next-to-ear has been discovered
     */
    private boolean mNear = false;
    /**
     * Sensor manager
     */
    private SensorManager mSensorManager=null;
    /**
	 * Ignore first covering event (Galaxy S3 bug workaround)
	*
	 * Galaxy S3 proximity sensor always provides FAR event as a first one, regardless
	 * sensor is actually covered or not. Thus, first NEAR event may be registered when phone is still 
	 * in the pocket, which causes unwanted call answer. 'delay' value changes does little effect, since 
	 * messages are asynchronous and the first NEAR event can be read after 100-4000 ms after buggy FAR one 
	 */
    private boolean mGalaxyS3Workaround = false;
    /**
     * Single instance - necessary for persistent sensor
     */
    private static ProximitySensorListener mInstance = null;
    /**
     * Instance was created as persistent
     */
    private static boolean mPersistent = false;
    
    /**
     * Create new or return existing sensor listener instance
     * @param context application context
     * @return sensor listener instance
     */
    public static ProximitySensorListener getSensorProxy(Context context) {
    	return getSensorProxy(context, false);
    }
    
    /**
     * Create new or return existing sensor listener instance
     * @param context application context
     * @param attach immediately start processing sensor events 
     * @return sensor listener instance
     */
    public static ProximitySensorListener getSensorProxy(Context context, boolean attach)
    {
    	if (mInstance == null) {
    		mInstance =  new ProximitySensorListener(context);
    		Log.l(TAG, "Created new instance: %s", mInstance);
    	} else 
    		Log.l(TAG, "Reusing existing instance: %s", mInstance);
    	if (attach)
    		mInstance.attach();
    	return mInstance;
    }
    
	/**
	 * Delete or create proximity sensor listener instance with new parameters when properties changed
	 * @param context application context
	 * @param persistent if <b>true</b>, new instance will be created, if <b>false</>, any existing one will be deleted
	 * @return proximity sensor listener instance
	 */
	public static ProximitySensorListener setPersistent(Context context, boolean persistent)
	{
		return setPersistent(context, persistent, false);
	}

	/**
	 * Delete or create proximity sensor listener instance with new parameters when properties changed
	 * @param context application context
	 * @param persistent if <b>true</b>, new instance will be created, if <b>false</>, any existing one will be deleted
	 * @param forceReload create a fresh instance even if one exists (due to parameters change) 
	 * @return proximity sensor listener instance
	 */
	public static ProximitySensorListener setPersistent(Context context, boolean persistent, boolean forceReload)
	{
		mPersistent = persistent;
		if (mPersistent) {
			// create new persistent instance, preserving 'attached' attribute value
			boolean attached = (mInstance != null && mInstance.mAttached);
			if (forceReload)
				// delete existing instance first
				ProximitySensorListener.deleteSensorProxy();
			return ProximitySensorListener.getSensorProxy(context, attached);
		} 
		else
			// just delete
			return ProximitySensorListener.deleteSensorProxy();
	}

	/**
	 * Unconditionally deletes an existing instance, freeing all resources
	 * @return
	 */
    private static ProximitySensorListener deleteSensorProxy()
    {
    	if (mInstance != null) {
    		mInstance.finalize();
    		Log.l(TAG, "Deleted instance %s", mInstance);
    		mInstance = null;
    	}
    	return null;
    }

    /**
     * If persistent instance was requested, just stop processing proximity sensor events; otherwise, delete instance
     */
	private static void closeSensorProxy() {
		if (mPersistent)
	    	if (mInstance != null)
	    		mInstance.detach();
		else
			ProximitySensorListener.deleteSensorProxy();
	}
	
	/**
	 * Create a new sensor listener object basing on settings chosen by user 
	 * @param context
	 */
    private ProximitySensorListener(Context context) {
		// Load preferences
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		mDelay = Integer.parseInt(prefs.getString("delay", "2000"));
		mGalaxyS3Workaround = prefs.getBoolean("galaxyS3_wo", false);
		// BETA: sensor delay value - may fix the problem with 'bad' FAR - 'good' NEAR events time gap 
		int delay = Integer.parseInt(prefs.getString("sensor_delay", String.valueOf(SensorManager.SENSOR_DELAY_NORMAL)));
	    
		// register for receiving proximity sensor events
		mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
		mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY), delay);

		Log.l(TAG, "Registered for events with delay %s", delayToStr(delay));
    }

    /**
     * Start analyzing sensor events
     */
	protected void attach() {
		// reset state variables 
		mAttached = true;
		mNear = false;
		// if GS3 WO was requested, ignore first NEAR event
		if (mGalaxyS3Workaround) 
			mNearCounter = -1;
		else
			mNearCounter = 0;
		// reset last registered sensor event timestamp
	    mTimestamp = (new Date()).getTime();
		Log.l(TAG, "Attached");
	}

	/**
	 * Stop analyzing sensor events - they are still received, but ignored
	 */
	protected void detach() {
		mAttached = false;
		Log.l(TAG, "Detached");
	}

	/**
	 * Instance wrapper for static method closeSensorProxy - API standardization with Bluetooth listener 
	 */
	public void close() {
		ProximitySensorListener.closeSensorProxy();
	}

	/**
	 * Convert SENSOR_DELAY_xxx constant to its string representation
	 * @param delay SENSOR_DELAY_xxx constant
	 * @return its string representation
	 */
	private String delayToStr(int delay)
    {
    	switch (delay) {
    	case SensorManager.SENSOR_DELAY_NORMAL:
    		return "SENSOR_DELAY_NORMAL";
    	case SensorManager.SENSOR_DELAY_UI:
    		return "SENSOR_DELAY_UI";
    	case SensorManager.SENSOR_DELAY_GAME:
    		return "SENSOR_DELAY_GAME";
    	case SensorManager.SENSOR_DELAY_FASTEST:
    		return "SENSOR_DELAY_FASTEST";
    	}
    	return "!!Unknown!!";
    }
    
	/**
	 * Phone has been put at user's ear
	 * @return value
	 */
    public boolean proximityDetected() {
    	return mNear;
    }
  
    /**
     * Check if sensor covering may be interpreted as putting the phone at user's ear
     * @param event sensor event
     * @return analysis result
     */
	private Boolean verifyNear(SensorEvent event)
	{
		// if this is a NEAR event
		if (isNear(event)) {
			// ignore first event on Galaxy S3 if the workaround was requested
			return (++mNearCounter > 0); 
		}
		return false;
	}
	
	/**
	 * Checks sensor event type
	 * @param event sensor event
	 * @return <b>true</b> for NEAR event, <b>false</b> otherwise
	 */
	private Boolean isNear(SensorEvent event)
	{
		return (event.values[0] <= 0.01);
	}

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	public void onSensorChanged(SensorEvent event) {
		// get event time from system clock - event.timestamp is a bit strange...
		long now = (new Date()).getTime();
		// process only if attached
		if(event.sensor.getType()==Sensor.TYPE_PROXIMITY && mAttached){
			Log.l(TAG, "Received status %s", (isNear(event) ? "NEAR" : "FAR"));
		    if ( verifyNear(event) && (now - mTimestamp > mDelay) ) {
				Log.l(TAG, "Phone at head!");
		    	mNear = true;
		    }
		}
		mTimestamp = now; 
	}
	
	/**
	 * Destructor
	 */
	protected void finalize()
	{
		mSensorManager.unregisterListener(this);
		Log.l(TAG, "Unregistered from events");
		try {
			super.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
