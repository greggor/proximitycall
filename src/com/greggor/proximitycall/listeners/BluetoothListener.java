/*
 * ProximityCall
 * Automatically answer call when headset is put against the ear.
 * 
 * Copyright (C) 2012 Grzegorz Ozanski (greggor@gmail.com). All rights reserved. 
 */
package com.greggor.proximitycall.listeners;
import java.util.List;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.BluetoothProfile.ServiceListener;
import android.content.Context;

/**
 * Attach to BluetoothProfile.HEADSET and listen for events. This is the proper way of 
 * detecting whether BT headset is connected or not on ICS and higher
 * 
 * @author Grzegorz Ozanski (greggor@gmail.com)
 *
 */
public class BluetoothListener implements ServiceListener {
	/**
	 * Bluetooth profile proxy
	 */
	BluetoothProfile mProxy;

	/**
	 * Check if BT headset is connected by polling BT proxy
	 * @return <b>true</b> if headset is connected, <b>false</b> otherwise
	 */
	public boolean isHeadsetConnected() {
		if (mProxy != null && mProxy instanceof BluetoothHeadset) {
			try {
				mProxy.getConnectedDevices();
	            List<BluetoothDevice> connectedDevices = mProxy.getConnectedDevices();
	            for (BluetoothDevice device : connectedDevices) {
	            	//TODO Unnecessary double check?
	            	if (mProxy.getConnectionState(device) == BluetoothProfile.STATE_CONNECTED) {
	            		return true;
	            	}
	            }
	        } catch (Exception e) {
	        }
		}
		return false;
	}

	/**
	 * Attach to headset Bluetooth profile
	 * @param context application context
	 */
	public BluetoothListener(Context context)
	{
		// Obtain proxy associated with BT headset profile
		BluetoothAdapter.getDefaultAdapter().getProfileProxy(context, this, BluetoothProfile.HEADSET);
	}
	
	public void onServiceConnected(int profile, BluetoothProfile proxy) {
		mProxy = proxy;
	}

	public void onServiceDisconnected(int profile) {
		mProxy = null;
	}

	/**
	 * Close proxy connection to avoid resources leaking
	 */
	public void close() {
		if (mProxy != null)
			BluetoothAdapter.getDefaultAdapter().closeProfileProxy(BluetoothProfile.HEADSET, mProxy);
	}

	/**
	 * Destructor 
	 */
	protected void finalize() {
		close();
		try {
			super.finalize();
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}
